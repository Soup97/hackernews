import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PostModule } from './post/post.module';
import { PostService } from './post/post.service';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/hacker_news_db', {
      useNewUrlParser: true,
    }),
    PostModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
