import { Injectable, HttpService } from '@nestjs/common';
import { Model, Query } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

import { Post } from './interfaces/post.interface';
import { CreatePostDTO } from './dto/post.dto';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios';

@Injectable()
export class PostService {
  constructor(
    @InjectModel('Post') private readonly postModel: Model<Post>,
    private httpService: HttpService,
  ) { }

  async getPostsFromAPI() {
    const posts = await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
    posts.data.hits.forEach((post) => {
      this.postModel.find({ objectID: post.objectID }).then((res) => {
        if (res.length <= 0) {
          if (post.title || post.story_title) {
            this.createPost(post);
          }
        }
      });
    });
    return posts.data.hits;
  }

  async getPosts(): Promise<Post[]> {
    const posts = await this.postModel.find();
    return posts;
  }

  async getPostByID(objectID: string) {
    const post = await this.postModel.find({ objectID });
    console.log(post);
    return post;
  }

  async createPost(createPostDTO: CreatePostDTO): Promise<Post> {
    const post = new this.postModel(createPostDTO);
    await post.save();
    return post;
  }

  async deletePost(postID: string): Promise<Post> {
    const deletedPost = await this.postModel.findByIdAndDelete(postID);
    return deletedPost;
  }
}
