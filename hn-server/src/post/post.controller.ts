import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Res,
  HttpStatus,
  Query,
  NotFoundException,
} from '@nestjs/common';
import { CreatePostDTO } from './dto/post.dto';
import { PostService } from './post.service';

@Controller('post')
export class PostController {
  constructor(private postService: PostService) { }

  @Post('/create')
  async createPost(@Res() res, @Body() createPostDTO: CreatePostDTO) {
    const post = await this.postService.createPost(createPostDTO);
    return res.status(HttpStatus.OK).json({
      message: 'Post Successfully Created',
      post,
    });
  }

  @Get('/')
  async getPosts(@Res() res) {
    const posts = await this.postService.getPosts();
    return res.status(HttpStatus.OK).json({
      posts,
    });
  }

  @Get('/fromAPI')
  async getPostsFromAPI(@Res() res) {
    const posts = await this.postService.getPostsFromAPI();
    return res.status(HttpStatus.OK).json({
      posts,
    });
  }

  @Delete('/delete')
  async deletePost(@Res() res, @Query('postID') postID) {
    const postDeleted = await this.postService.deletePost(postID);
    if (!postDeleted) throw new NotFoundException('Post Does not exists');
    return res.status(HttpStatus.OK).json({
      message: 'Post Deleted Successfully',
      postDeleted,
    });
  }
}
