import { Document } from 'mongoose';

export interface Post extends Document {
  readonly story_title: string;
  readonly story_url: string;
  readonly objectID: string;
  readonly title: string;
  readonly subtitle: string;
  readonly createdAt: Date;
  readonly url: string;
}
