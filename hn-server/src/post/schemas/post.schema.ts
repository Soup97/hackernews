import { Schema } from 'mongoose';

export const PostSchema = new Schema({
  title: String,
  objectID: String,
  story_title: String,
  story_url: String,
  subtitle: String,
  url: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});
